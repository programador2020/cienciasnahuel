
package cienciasnahuel;

import java.util.ArrayList;
import java.sql.*;


public class CienciasNahuel {


    public static void main(String[] args) {
        System.out.println("Hola");
        //Alumnos
        ArrayList listadoAlumnos = new ArrayList();
        Alumnos miAlumno1 = new Alumnos();
        miAlumno1.nombre = "Luis";
        miAlumno1.apellido = "Sekh";
        
        Alumnos miAlumno2 = new Alumnos();
        miAlumno2.nombre = "Veronica";
        miAlumno2.apellido = "Balder";
        listadoAlumnos.add(miAlumno1);
        listadoAlumnos.add(miAlumno2);
        
        System.out.println(listadoAlumnos);
        
        
        
        //Materias
        ArrayList listadoMaterias = new ArrayList();
        Materias miMateria1 = new Materias();
        miMateria1.nombre = "matematica";
        Materias miMateria2 = new Materias();
        miMateria2.nombre = "Literatura";
        Materias miMateria3 = new Materias();
        miMateria3.nombre = "programación";
        listadoMaterias.add(miMateria1);
        listadoMaterias.add(miMateria2);
        listadoMaterias.add(miMateria3);
        System.out.println(listadoMaterias);
        
        //Inscripcion
        Inscripciones miInscripcion1 = new Inscripciones();
        miInscripcion1.alumno = miAlumno2;
        miInscripcion1.listado.add(miMateria1);
        miInscripcion1.listado.add(miMateria3);
        miInscripcion1.listado.add(miMateria2);
        
        System.out.println("Primera inscripcion de la plataforma de Nahuel!" );
        System.out.println(miInscripcion1);
        
        //Conexion a la base de datos
        Connection con = null;
        String usuario = "cine";
        String clave = "cine";
        String host = "jdbc:mysql://localhost:3306/ciencias_nahuel";
        String consultaSql = "SELECT * FROM alumnos;";
        try {
            con = DriverManager.getConnection(host, usuario, clave);
            PreparedStatement pstm = con.prepareStatement(consultaSql);
            ResultSet rs = pstm.executeQuery();
            while (rs.next()) {
                System.out.println("Nombre: " + rs.getString("nombre") + " Email: " + rs.getString("email"));
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        } finally {
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException ex) {
                    System.out.println(ex.getMessage());
                }
            }
        }

        
        
        System.out.println("Chau");
    }
    
}
