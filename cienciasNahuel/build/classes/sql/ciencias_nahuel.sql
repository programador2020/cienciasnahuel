-- phpMyAdmin SQL Dump
-- version 4.6.6deb4
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 25-05-2020 a las 18:10:22
-- Versión del servidor: 10.1.37-MariaDB-0+deb9u1
-- Versión de PHP: 7.0.33-0+deb9u7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `ciencias_nahuel`
--
CREATE DATABASE IF NOT EXISTS `ciencias_nahuel` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `ciencias_nahuel`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `alumnos`
--

CREATE TABLE `alumnos` (
  `nombre` varchar(255) NOT NULL,
  `apellido` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `asistencia` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `alumnos`
--

INSERT INTO `alumnos` (`nombre`, `apellido`, `email`, `asistencia`) VALUES
('Luis', 'Seck', 'luis@gmail.com', 'presente'),
('Veronica', 'Valder', 'vero@gmail.com', 'presente'),
('Pedro', 'Gomez', 'pedro@gmail.com', 'ausente'),
('Raul', 'Rich', 'raul@gmail.com', 'ausente'),
('Victoria', 'Peres', 'vero@gmail.com', 'presente');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `inscripciones`
--

CREATE TABLE `inscripciones` (
  `alumno` varchar(255) DEFAULT NULL,
  `materia` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `materias`
--

CREATE TABLE `materias` (
  `nombre` varchar(255) NOT NULL,
  `descripcion` varchar(255) NOT NULL,
  `horario` varchar(255) NOT NULL,
  `docente` varchar(255) NOT NULL,
  `ciclo` varchar(255) NOT NULL,
  `anio` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `materias`
--

INSERT INTO `materias` (`nombre`, `descripcion`, `horario`, `docente`, `ciclo`, `anio`) VALUES
('Matematica', 'Aca va la descripcion de la materia', 'Lunes y Viernes 23:00', 'Joquin Gomez', 'cuatrimestral', 1),
('Literatura', 'Aca va la descripcion de la materia', 'Martes y Jueves 17.30', 'Fernandez Lautaro', 'Anual', 1),
('Programacion', 'Aca va la descripcion de la materia', 'Lunes 23:00', 'Pedro', 'cuatrimestral', 2),
('HTML', 'Aca va la descripcion de la materia', 'Miercoles a 14', 'Luisa', 'cuatrimestral', 2),
('Base de datos', 'Aca va la descripcion de la materia', 'Lu, Mar y Ju de 17 a 21', 'Silvina', 'Anual', 3);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
